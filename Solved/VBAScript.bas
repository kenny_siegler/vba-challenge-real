Attribute VB_Name = "Module1"
Sub Clean()

    Dim ws As Worksheet
    'setting column variables
    Dim ticker, yc, pc, tv, x As Integer
    ticker = 9
    yc = 10
    pc = 11
    tv = 12

    
    Dim start_value As Double
    
    Dim volume As Double
    
   'iterating through worksheets
    For Each ws In Worksheets
        x = 1
        volume = 0
        'iterating through rows
        For Row = 1 To 31000
        
        Dim new_ticker As String
        
            'Getting new tickers
            If ws.Cells(Row, 1).Value <> "<ticker>" Then
                volume = volume + ws.Cells(Row, 7).Value
                ws.Cells(x, tv).Value = volume
            End If
            If ws.Cells(Row, 1).Value <> ws.Cells(Row + 1, 1).Value Then
                
                volume = 0
                start_value = next_ticker_start_value
                new_ticker = ws.Cells(Row, 1).Value
                ws.Cells(x, ticker).Value = new_ticker
                
                x = x + 1
                
                'Bug fixing to make sure the headers didn't end up in the second table
                If IsNumeric(ws.Cells(Row, 6)) Then
                    end_value = ws.Cells(Row, 6).Value
                    
                End If
                If IsNumeric(ws.Cells(Row + 1, 3).Value) Then
                   next_ticker_start_value = ws.Cells(Row + 1, 3).Value
                
                End If
               
               Dim year_change As Double
               Dim percent_change As Double
               
               'calc yearly change and % change
               year_change = Round(start_value - end_value, 2)
                'Fixing overflow error
                If start_value = 0 Then
                    percent_change = 0
                Else
                    percent_change = year_change / start_value
                End If
                
                ws.Cells(x - 1, yc).Value = year_change
                
                ws.Cells(x - 1, pc).Value = percent_change
                
                ' conditional formatting
                If year_change >= 0 Then
                    ws.Cells(x - 1, yc).Interior.ColorIndex = 4
                    ws.Cells(x - 1, pc).Interior.ColorIndex = 4
                Else
                    ws.Cells(x - 1, yc).Interior.ColorIndex = 3
                    ws.Cells(x - 1, pc).Interior.ColorIndex = 3
                End If
                
        
            End If
                
        Next Row
   
    x = 1
    'For loop to determine our biggest gains/losses
    Max = 0
    Min = 0
    vol = 0
        For r = 2 To 100
           
           If ws.Cells(r, pc).Value > Max Then
                max_tick = ws.Cells(r, ticker).Value
                Max = ws.Cells(r, pc).Value
            End If
            If ws.Cells(r, pc).Value < Min Then
                min_tick = ws.Cells(r, ticker).Value
                Min = ws.Cells(r, pc).Value
            End If
            If ws.Cells(r, tv).Value > vol Then
                vol_tick = ws.Cells(r, ticker).Value
                vol = ws.Cells(r, tv).Value
            End If
        Next r
    
    'Set Headers for pages
    ws.Cells(x, ticker).Value = "Ticker"
    ws.Cells(x, yc).Value = "Yearly Change"
    ws.Cells(x, pc).Value = "Percent Change"
    ws.Cells(x, tv).Value = "Total Volume"
    
    'Set Headers for max/min columns
    ws.Cells(1, 16).Value = "Ticker"
    ws.Cells(1, 17).Value = "Value"
    
    'max gain
    ws.Cells(2, 15).Value = "Greatest Percentage Increase"
    ws.Cells(2, 16).Value = max_tick
    ws.Cells(2, 17).Value = FormatPercent(Max, 2)
    
    'max loss
    ws.Cells(3, 15).Value = "Greatest Percentage Decrease"
    ws.Cells(3, 16).Value = min_tick
    ws.Cells(3, 17).Value = FormatPercent(Min, 2)
    
    'greatest volume
    ws.Cells(4, 15).Value = "Greatest Volume"
    ws.Cells(4, 16).Value = vol_tick
    ws.Cells(4, 17).Value = vol
    

    
    'reset our values
    start_value = 0
    end_value = 0
    next_ticker_start_value = 0
    
    Next ws
    
    
   
End Sub


Sub Clean1()

    Dim ws As Worksheet
    
    Dim ticker, yc, pc, tv, x As Integer
    ticker = 9
    yc = 10
    pc = 11
    tv = 12

    x = 1
    For Each ws In Worksheets
            
        i = 2
        For Row = 1 To 31000
        Dim new_ticker As String
            
            If ws.Cells(Row, 1).Value <> ws.Cells(Row + 1, 1).Value Then
            
                start_value = next_ticker_start_value
                new_ticker = ws.Cells(Row, 1).Value
                ws.Cells(x, ticker).Value = new_ticker
                

                
            End If
        Next Row
    x = 1
    ws.Cells(x, ticker).Value = "Ticker"
    ws.Cells(x, yc).Value = "Yearly Change"
    ws.Cells(x, pc).Value = "Percent Change"
    ws.Cells(x, tv).Value = "Total Volume"
    
    Next ws
    
    
   
End Sub
